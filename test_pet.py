import allure
import requests

@allure.feature("冒烟测试")
class TestPet:
    def setup_class(self):
        # 基础的数据信息
        pass

    @allure.story("添加宠物接口用例")
    def test_postpet(self):
        url ="https://petstore.swagger.io/v2/pet"
        headers = {"content-Type":"application/json"}
        data = {
            "id" : 7,
            "name" : "Max",
            "photoUrls" :[],
            "status" : "available"
            }
        response = requests.post(url, headers=headers,json=data)
        assert response.status_code == 200

    @allure.story("查询宠物接口冒烟用例")
    def test_getpet(self):
        with allure.step("发出查询接口请求"):
            r = requests.get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
        with allure.step("获取查询接口响应"):
            print(r.json())
        with allure.step("查询接口断言"):
            assert r.status_code == 200
